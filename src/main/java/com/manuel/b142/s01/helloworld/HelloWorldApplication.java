package com.manuel.b142.s01.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class HelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}
	@GetMapping("api/hello")
	public String hello(){
		return String.format("Hello, world! Welcome to Java + Spring Boot.");
	}
	@GetMapping("api/hello-name")

	//annotation to receive from URL
	//http://localhost:8080/api/hello-name?name=Lebron
	public String helloName(@RequestParam(value = "name", defaultValue="world") String name) {
		return String.format("Hello, %s! Welcome to Java + Spring Boot.", name);
	}

	// http://localhost:8080/api/your-name/<name_input>

	@GetMapping("api/hi/{user}")
	public String hiUser(@PathVariable("user") String user) {
		return String.format("Hello, %s", user);
	}
	@GetMapping("api/your-name/{name}")
	public String yourName(@PathVariable("name") String name) {
		return String.format("Hello, %S! How are you doing today?", name);
	}

	@GetMapping("api/good-evening")
	public String goodEvening() {
		return String.format("Good Evening!");
	}

}
